var MyLayer = cc.Layer.extend({
    helloLabel:null,
    sprite:null,
    init:function () {
        this._super();
        var size = cc.director.getWinSize();
        var drow = new cc.DrawNode();

        drow.drawDot(cc.p(0,0),10,cc.color(255,255,255,255));
        this.addChild(drow);
        drow.setPositionX(size.width/2);
        drow.setPositionY(size.height/2);
        var angle = 0;
        this.schedule(function () {
            drow.setPositionY(size.height/2 + Math.cos(angle)*100);
            drow.setPositionX(size.width/2 + Math.sin(angle)*80);
            angle += 0.1;
        });
        var closeItem = new cc.MenuItemImage(
            s_CloseNormal,
            s_CloseSelected,
            function () {
                cc.log("close");
            },this);

        closeItem.setAnchorPoint(0.5, 0.5);
        var menu = new cc.Menu(closeItem);
        menu.setPosition(0, 0);
        //this.addChild(menu, 1);

        closeItem.setPosition(size.width/2, size.height/2+150);
        this.helloLabel = new cc.LabelTTF("Hello World", "Impact", 38);
        this.helloLabel.setPosition(size.width / 2, size.height - 40);

        this.sprite = new cc.Sprite(s_HelloWorld);
        this.sprite.setAnchorPoint(0.5, 0.5);
        this.sprite.setPosition(size.width / 2, size.height / 2);
        this.sprite.setScale(size.height / this.sprite.getContentSize().height);
        //this.addChild(this.sprite, 0);
    }
});

var MyScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new MyLayer();
        this.addChild(layer);
        layer.init();
    }
});
